import React, { useEffect } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import Header from './components/header/header';
import HomePage from './pages/homepage/homepage'
import SignInAndSignUpPage from "./pages/sign-in-and-sign-up/sign-in-and-sign-up";
import ShopPage from './pages/shop/shop';
import CheckoutPage from './pages/checkout/checkout';

import { selectCurrentUser } from './redux/user/user-selectors';
import { checkUserSession } from './redux/user/user-actions';

import { GlobalStyle } from './global-styles';

const App = ({ checkUserSession, currentUser }) => {
    useEffect(() => {
        checkUserSession()
    }, [checkUserSession]);

    return (
        <div>
            <GlobalStyle />
            <Header />
            <Switch> {/* moment a route inside finds a match, it doesn't render anything else but that route.*/}
                <Route exact path='/' component={HomePage} /> {/* if exact is not present, both homepage and hats page would render here*/}
                <Route path='/shop' component={ShopPage} />
                <Route exact
                       path='/signIn'
                       render={() => currentUser ? (<Redirect to='/' />) : <SignInAndSignUpPage />}
                />
                <Route exact path='/checkout/' component={CheckoutPage}></Route>
            </Switch>
        </div>
    );
}


const mapDispatchToProps = dispatch => ({
    checkUserSession: () => dispatch(checkUserSession())
});

const mapStateToProps = createStructuredSelector({
    currentUser: selectCurrentUser,
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
