import React, { useState } from 'react';
import { connect } from 'react-redux';

import FormInput from '../form-input/form-input';
import CustomButton from '../custom-button/custom-button';

import { googleSignInStart } from '../../redux/user/user-actions';
import { emailSignInStart } from '../../redux/user/user-actions';
import './sign-in.scss'

const SignIn = ({ emailSignInStart, googleSignInStart }) => {
    const [userCredentials, setCredentials] = useState({email: '', password: ''});

    const { email, password } = userCredentials;

    const handleSubmit = async event => {
        event.preventDefault();

        const { email, password } = userCredentials;

        emailSignInStart(email, password);
        // try{
        //     await auth.signInWithEmailAndPassword(email, password);
        //     this.setState({ email: '', password: ''});
        // } catch (error) {
        //     console.log(error);
        // }
        // this.setState({ email: '', password: ''})
    };

    const handleChange = event => {
        const { value, name } = event.target;
        setCredentials({ ...userCredentials, [name]: value })
    };

    return (
        <div className='sign-in'>
            <h2>I already have an account</h2>
            <span>Sign in with your email and password</span>

            <form onSubmit={handleSubmit}>
                <FormInput
                    name='email'
                    value={email}
                    handleChange={handleChange}
                    label='Email'
                    required/>
                <FormInput
                    name='password'
                    type='password'
                    value={password}
                    handleChange={handleChange}
                    label='Password'
                    required
                />
                <div className='buttons'>
                    <CustomButton type='submit' value="Submit">Sign in</CustomButton>
                    <CustomButton
                        type ='button'
                        onClick={googleSignInStart}
                        isGoogleSignIn
                    >
                        {' '}
                        Sign in with Google{' '}
                    </CustomButton>
                </div>

            </form>
        </div>
    );
}


const mapDispatchToProps = dispatch => ({
    googleSignInStart: () => dispatch(googleSignInStart()),
    emailSignInStart: (email, password) => dispatch(emailSignInStart({ email, password}))
});

export default connect(null, mapDispatchToProps)(SignIn);